import React from "react";

export default function Contact() {
  let contact = [
    {
      icon: <i className="fa fa-envelope "></i>,
      text: "thienha16298@gmail.com",
      href: "#",
    },
    {
      icon: <i class="fa fa-mobile-alt"></i>,
      text: "0773765755",
      href: "#",
    },
    {
      icon: <i class="fab fa-github "></i>,
      text: "@thienha1602",
      href: "https://gitlab.com/ThienHa1602",
    },
    {
      icon: <i class="fab fa-facebook "></i>,
      text: "Thiênn Hà",
      href: "https://www.facebook.com/profile.php?id=100010330686856",
    },
    {
      icon: <i class="fab fa-instagram "></i>,
      text: "@galaxy_ne",
      href: "https://www.instagram.com/galaxy_ne/",
    },

    {
      icon: <i class="fa fa-map-marker-alt"></i>,
      text: "Go Vap District, Ho Chi Minh City",
      href: "https://goo.gl/maps/YG9vQ15xC8BiaR6QA",
    },
  ];
  return (
    <div className="bg-neutral-800 px-4 py-16 md:py-24 lg:px-8" id="Contact">
      <div className="mx-auto max-w-screen-lg">
        <div className="flex flex-col gap-y-6">
          <div className="flex flex-col gap-6 md:flex-row md:items-center">
            <i className="fa fa-envelope text-5xl shadow-xl text-white"></i>
            <h2 className="text-2xl font-bold text-white">Get in touch.</h2>
          </div>
          <div className="grid grid-cols-1 gap-6 md:grid-cols-2">
            <div className="col-span-1">
              <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d489.82344948256593!2d106.67843037295685!3d10.842838828875527!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x317529803286e43d%3A0xf077daa38cacd70f!2zNDMvMi8xMSDEkC4gU-G7kSAyNywgUGjGsOG7nW5nIDYsIEfDsiBW4bqlcCwgVGjDoG5oIHBo4buRIEjhu5MgQ2jDrSBNaW5oLCBWaeG7h3QgTmFt!5e0!3m2!1svi!2s!4v1683557291619!5m2!1svi!2s"
                width={500}
                height={350}
                style={{ border: 0 }}
                allowFullScreen
                loading="lazy"
                referrerPolicy="no-referrer-when-downgrade"
              />
            </div>
            <div className="text-left text-neutral-300 col-span-1 flex flex-col gap-y-8 ">
              <h2 className="text-2xl leading-6 ">
                Here is all my information for you to contact.
              </h2>
              <div className="flex flex-col space-y-4 text-base  sm:space-y-2">
                {contact.map(({ icon, text, href }) => {
                  return (
                    <div className="flex items-center">
                      <a
                        className="-m-2 flex rounded-md p-2 text-neutral-300 hover:text-orange-500 focus:outline-none focus:ring-2 focus:ring-orange-500"
                        href={href}
                        target="_blank"
                      >
                        {icon}
                        <span className="ml-3 text-sm sm:text-base">
                          {text}
                        </span>
                      </a>
                    </div>
                  );
                })}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
