import React from "react";
import Education from "./Education/Education";
import Work from "./Work/Work";
import Skill from "./Skill";

export default function Resume() {
  return (
    <div className="bg-neutral-100 px-4 py-16 md:py-24 lg:px-8" id="Resume">
      <div className="mx-auto max-w-screen-lg">
        <div className="flex flex-col divide-y-2 divide-neutral-300">
          <Education />
          <Work />
          <Skill />
        </div>
      </div>
    </div>
  );
}
