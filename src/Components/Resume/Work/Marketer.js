import React from "react";

export default function Marketer() {
  return (
    <div className="flex flex-col pb-8 text-center last:pb-0 md:text-left">
      <div className="flex flex-col pb-4">
        <h2 className="text-xl font-bold">Marketer</h2>
        <div className="flex items-center justify-center gap-x-2 md:justify-start">
          <span className="flex-1 text-sm font-medium italic sm:flex-none">
            NGOI SAO SANG COMPANY LIMITED
          </span>
          <span>•</span>
          <span className="flex-1 text-sm sm:flex-none">10/2021 - 8/2022</span>
        </div>
      </div>
      <p>
        Create campaigns to increase profits. Contact KOLs/Models to carry out
        brand promotion campaigns. Create content, monthly promotions
      </p>
    </div>
  );
}
