import React from "react";

export default function Skill() {
  let skill = [
    "REACTJS",
    "API",
    "HTML",
    "CSS",
    "GIT",
    "JAVASCRIPT",
    "TYPESCRIPT",
    "REDUX",
    "REDUX THUNK",
    "JQUERY",
    "TAILWIND",
    "BS4",
    "ANTD",
  ];
  return (
    <div className="grid grid-cols-1 gap-y-4 py-8 first:pt-0 last:pb-0 md:grid-cols-4">
      <div className="col-span-1 flex justify-center md:justify-start">
        <div className="relative h-max">
          <h2 className="text-xl font-bold uppercase text-neutral-800">
            Skills
          </h2>
          <span className="absolute inset-x-0 -bottom-1 border-b-2 border-orange-400" />
        </div>
      </div>
      <div className="col-span-1 grid gap-4 grid-cols-5 md:col-span-3">
        {skill.map((item) => {
          return (
            <div
              style={{ backgroundColor: "#e4ebe4" }}
              className=" text-black  font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2"
            >
              {item}
            </div>
          );
        })}
      </div>
    </div>
  );
}
