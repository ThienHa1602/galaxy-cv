import React from "react";

export default function Huflit() {
  return (
    <div className="flex flex-col pb-8 text-center last:pb-0 md:text-left">
      <div className="flex flex-col pb-4">
        <h2 className="text-xl font-bold">
          Bachelor of Business Administration
        </h2>
        <div className="flex items-center justify-center gap-x-2 md:justify-start">
          <span className="flex-1 text-sm font-medium italic sm:flex-none">
            HUFLIT University
          </span>
          <span>•</span>
          <span className="flex-1 text-sm sm:flex-none">2016-2020</span>
        </div>
      </div>
      <p>
        In university, I learned about how the economy works and how to analyze
        the market. Teamwork, presentation, and data research are important
        skills I learned when I was in university.
      </p>
    </div>
  );
}
