import React from "react";

export default function CyberSoft() {
  return (
    <div className="flex flex-col pb-8 text-center last:pb-0 md:text-left">
      <div className="flex flex-col pb-4">
        <h2 className="text-xl font-bold">Profesional Front-End Developer</h2>
        <div className="flex items-center justify-center gap-x-2 md:justify-start">
          <span className="flex-1 text-sm font-medium italic sm:flex-none">
            CyberSoft Academy
          </span>
          <span>•</span>
          <span className="flex-1 text-sm sm:flex-none">9/2022 - 4/2023</span>
        </div>
      </div>
      <p>
        Start exposure and learning programming languages. The main programming
        languages ​​learned are JavaScripts. I trained and participated in many
        different projects. Learn and be able to use multiple libraries and
        different tools.
      </p>
    </div>
  );
}
