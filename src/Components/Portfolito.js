import React from "react";

export default function Portfolito() {
  let portfolioItems = [
    {
      title: "Project Samar",
      url: "https://sa-mar.vercel.app/",
      image: "./Image/samar.png",
    },
    {
      title: "Project Restaurant",
      url: "https://restaurant-galaxy.vercel.app/",
      image: "./Image/dinner.png",
    },
    {
      title: "Project TheFour",
      url: "https://thefour-galaxy.vercel.app/",
      image: "./Image/thefour.png",
    },

    {
      title: "Project Employee Management",
      url: "https://qlnv-azure.vercel.app/",
      image: "./Image/QLNV.png",
    },
    {
      title: "Project Movie",
      url: "https://capstone-js-movie.vercel.app/",
      image: "./Image/movie.png",
    },
    {
      title: "Project Meipaly",
      url: "https://meipaly-khaki.vercel.app/",
      image: "./Image/maipaly.png",
    },
    {
      title: "Project TodoList",
      url: "https://todolist-form.vercel.app/",
      image: "./Image/todo.png",
    },
  ];
  return (
    <div
      className="bg-neutral-800 px-4 py-16 md:py-24 lg:px-8 text-left text-white"
      id="Portfolio"
    >
      <div className="mx-auto max-w-screen-lg">
        <div className="flex flex-col gap-y-8">
          <h2 className="self-center text-xl font-bold text-white">
            Check out some of my work
          </h2>
          <div className=" w-full columns-2">
            {portfolioItems.map((item, index) => {
              const { title, image, url } = item;
              return (
                <div className="pb-6" key={index}>
                  <div className="relative h-max w-full overflow-hidden rounded-lg shadow-lg shadow-black/30 lg:shadow-xl">
                    <img src={image} alt="" />
                    <a
                      href={url}
                      className="absolute inset-0 h-full w-full  bg-gray-900 transition-all duration-300 opacity-0 hover:opacity-80 "
                      target="#"
                    >
                      <div className="relative h-full w-full p-4">
                        <h2 className="text-center font-bold text-white opacity-100">
                          {title}
                        </h2>
                      </div>
                    </a>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
}
