import React from "react";
import Lottie from "lottie-react";
import bg_animate from "../Assets/sendmail_animate.json";

export default function Spacing() {
  return (
    <div className="bg-neutral-100 px-4 py-16 md:py-24 lg:px-8">
      <div className="flex justify-center">
        <Lottie style={{ width: 200 }} animationData={bg_animate} loop={true} />
      </div>
    </div>
  );
}
