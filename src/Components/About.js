import React from "react";

export default function About() {
  let aboutItems = [
    {
      label: "Location",
      text: "Go Vap District",
      Icon: <i class="fa fa-map-marker-alt"></i>,
    },
    { label: "Age", text: "25", Icon: <i class="fa fa-calendar-alt"></i> },
    {
      label: "Nationality",
      text: "Viet Nam",
      Icon: <i class="fa fa-flag"></i>,
    },
    {
      label: "Interests",
      text: "Football, Sing, Travel",
      Icon: <i class="fa fa-star"></i>,
    },
    {
      label: "Study",
      text: "HUFLIT University",
      Icon: <i class="fa fa-book-open"></i>,
    },
  ];
  return (
    <div
      className="bg-neutral-800 px-4 py-16 md:py-24 lg:px-8 text-left"
      id="About"
    >
      <div className="mx-auto max-w-screen-lg">
        <div className="grid grid-cols-1 gap-y-4 md:grid-cols-4">
          <div className="col-span-1 flex justify-center md:justify-start">
            <div className=" h-24 w-24 overflow-hidden rounded-xl md:h-32 md:w-32">
              <img src="./Image/avatarabout.png" alt="" />
            </div>
          </div>
          <div className="col-span-1 flex flex-col gap-y-6 md:col-span-3">
            <div className="flex flex-col gap-y-2">
              <h2 className="text-2xl font-bold text-white">About me</h2>
              <p className=" text-gray-300 ">
                Although I'm just a fresher, I have never been afraid of
                challenges and I'm always determined to strive to achieve the
                set goals. Inquisitive spirit and research ability are two
                things that I am always proud of. My career objective is to try
                to learn, gain experience and promote my forte to contribute to
                the company’s prosperity.
              </p>
            </div>
            <ul className="grid grid-cols-1 gap-4 sm:grid-cols-2">
              {aboutItems.map(({ label, text, Icon }, index) => (
                <li
                  className="col-span-1 flex text-white  items-start gap-x-2"
                  key={index}
                >
                  {Icon}
                  <span className="text-sm font-bold text-white">{label}:</span>
                  <span className=" text-sm text-gray-300">{text}</span>
                </li>
              ))}
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
}
