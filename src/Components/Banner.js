import React from "react";

export default function Banner() {
  return (
    <div>
      <div className=" flex h-screen w-screen items-center justify-center">
        <img
          className="absolute h-screen w-screen top-0 left-0 z-0 object-cover"
          src="./Image/banner-bg.png"
          alt=""
        />
        <div className="z-10  max-w-screen-lg px-4 lg:px-0">
          <div className="flex flex-col items-center gap-y-6  rounded-xl bg-gray-800/40 p-6 text-center shadow-lg backdrop-blur-sm">
            <h1 className="text-4xl font-bold text-white sm:text-5xl lg:text-7xl">
              Hi, I'm Thien Ha <br /> (a.k.a Galaxy)
            </h1>
            <p className=" text-stone-200  ">
              I'm a{" "}
              <strong className="text-stone-100">
                {" "}
                Junior Front End / ReactJS Software Engineer
              </strong>
              . I just graduated from{" "}
              <strong className="text-stone-100">
                {" "}
                CyberSoft technology school{" "}
              </strong>
              , but I have practiced and participated in many projects (you can
              see in my Portfolio). I can use many tech-skills such as:
              <strong className="text-stone-100">
                {" "}
                HTML, CSS, API, REDUX, REDUX-THUNK, REACTJS, REACT-HOOK, GIT,...{" "}
              </strong>{" "}
              and various libraries (
              <strong className="text-stone-100">
                antd, tailwind, BS4, moment, jQuerry,...
              </strong>
              )
            </p>
            <p className=" text-stone-200 ">
              In my free time, you can catch me training{" "}
              <strong className="text-stone-100">
                {" "}
                my ReactJS, Javascript skills
              </strong>
              , playing <strong className="text-stone-100">
                {" "}
                football
              </strong>{" "}
              with my friend, or traveling around{" "}
              <strong className="text-stone-100"> VietNam</strong>.
            </p>
            <div className="flex gap-x-4 text-neutral-100">
              <a
                className="-m-1.5 rounded-md p-1.5 transition-all duration-300 hover:text-orange-500 focus:outline-none focus:ring-2 focus:ring-orange-500  sm:-m-3 sm:p-3"
                href="https://gitlab.com/ThienHa1602"
                target="_blank"
              >
                <i class="fab fa-github fa-2xl"></i>
              </a>
              <a
                className="-m-1.5 rounded-md p-1.5 transition-all duration-300 hover:text-orange-500 focus:outline-none focus:ring-2 focus:ring-orange-500  sm:-m-3 sm:p-3"
                href="https://www.facebook.com/profile.php?id=100010330686856"
                target="_blank"
              >
                <i class="fab fa-facebook fa-2xl"></i>
              </a>
              <a
                className="-m-1.5 rounded-md p-1.5 transition-all duration-300 hover:text-orange-500 focus:outline-none focus:ring-2 focus:ring-orange-500  sm:-m-3 sm:p-3"
                href="https://www.instagram.com/galaxy_ne/"
                target="_blank"
              >
                <i class="fab fa-instagram fa-2xl"></i>
              </a>
            </div>
            <div>
              <a
                className="flex gap-x-2 rounded-full border-2 bg-none py-2 px-4 text-sm font-medium text-white ring-offset-gray-700/80 hover:bg-gray-700/80 focus:outline-none focus:ring-2 focus:ring-offset-2 sm:text-base border-orange-500 ring-orange-500"
                href="https://drive.google.com/file/d/1J6k28aDenuN4awSEEKTbWAjwEekGs4af/view?usp=sharing"
                target="blank"
              >
                Resume
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeWidth={2}
                  stroke="currentColor"
                  aria-hidden="true"
                  className="h-5 w-5 text-white sm:h-6 sm:w-6"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M4 16v1a3 3 0 003 3h10a3 3 0 003-3v-1m-4-4l-4 4m0 0l-4-4m4 4V4"
                  />
                </svg>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
