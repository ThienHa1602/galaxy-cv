import React from "react";
import { Link } from "react-scroll";

export default function Header() {
  const baseClass =
    "m-1.5 p-1.5 rounded-md font-bold hover:transition-colors hover:duration-300 hover:cursor-pointer focus:outline-none focus-visible:ring-2 focus-visible:ring-orange-500 sm:hover:text-orange-500 text-neutral-100";
  return (
    <header
      className="fixed top-0 z-50 hidden w-full bg-neutral-900/50 p-4 backdrop-blur sm:block"
      id="header"
    >
      <ul className="flex justify-center gap-x-8">
        <li>
          <Link activeClass="active" to="About" spy={true} smooth={true}>
            <span className={baseClass}>About</span>
          </Link>
        </li>
        <li>
          <Link to="Resume" spy={true} smooth={true}>
            <span className={baseClass}>Resume</span>
          </Link>
        </li>
        <li>
          <Link to="Portfolio" spy={true} smooth={true}>
            <span className={baseClass}>Portfolio</span>
          </Link>
        </li>
        <li>
          <Link to="Contact" spy={true} smooth={true}>
            <span className={baseClass}> Contact</span>
          </Link>
        </li>
      </ul>
    </header>
  );
}
