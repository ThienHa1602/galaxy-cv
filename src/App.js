import logo from "./logo.svg";
import "./App.css";
import Header from "./Components/Header";
import Banner from "./Components/Banner";
import About from "./Components/About";
import Resume from "./Components/Resume/Resume";
import Portfolito from "./Components/Portfolito";
import Contact from "./Components/Contact";
import Spacing from "./Components/Spacing";

function App() {
  return (
    <div className="App">
      <Header />
      <Banner />
      <About />
      <Resume />
      <Portfolito />
      <Spacing />
      <Contact />
    </div>
  );
}

export default App;
